from pathlib import Path
import os
import argparse
import random
import numpy as np
import glob
from PIL import Image
from tqdm import tqdm
# from sklearn.cluster import KMeans
# from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix, f1_score, accuracy_score
from sklearn import neighbors
# from cuml import neighbors
from utils.deep_feature_extractor import Img2Vec
import pandas as pd

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument('-i', "--insertion_crops", type=str,
                default="./gesture_crops/insertion_crops/found",
                help="Directory where the crops are stored")

ap.add_argument('-r', "--removal_crops", type=str,
                default="./gesture_crops/removal_crops/found",
                help="Directory where the crops are stored")

ap.add_argument('-m', "--model", choices=['resnet-50', 'resnet-18', 'alexnet'],
                default='resnet-50', help="Which model to use as feature extractor")

args = vars(ap.parse_args())

insertion_path = Path(args['insertion_crops'])  # store the directory path for insertion crops
total_inserted_items = os.listdir(insertion_path)  # all the items in the insertion gestures

removal_path = Path(args['removal_crops'])  # store the directory path for removal crops
total_removed_items = os.listdir(removal_path)  # all the items in the removal gestures

print("Total inserted items {}".format(total_inserted_items))
print("Total removed items {}".format(total_removed_items))

assert total_inserted_items == total_removed_items



sample_items = [2, 4, 6] # 10, 15, 20, 25, 30, 50]
sample_items_rate = 10

# model to use
img2vec = Img2Vec(model=args["model"], cuda=True)
vec_length = img2vec.layer_output_size


def majority_vote(predictions):
    return max(zip(*np.unique(predictions, return_counts=True)), key=lambda x: x[1])[0]


def get_random_sample_of_items(total_files, sample_size):
    return random.sample(total_files, k=sample_size)


def create_dataset(input_path, in_cart_items):
    ranges = []
    labels = np.array([], dtype=np.uint)
    image_paths = []
    start = 0

    for i in range(len(in_cart_items)):
        paths = glob.glob(os.path.join(input_path / in_cart_items[i], '*rgb.png'))
        print(len(paths))
        image_paths += paths
        labels = np.concatenate((labels, np.full(shape=len(paths), fill_value=i, dtype=np.uint)))
        ranges.append((start, start + len(paths)))
        start = ranges[-1][-1]

    return ranges, labels, image_paths


def create_dataframe(sample_items, number_of_trails):
    return pd.DataFrame(index=sample_items, columns=[i+1 for i in range(number_of_trails)])


def create_deep_features(image_paths, vec_length, batch_size=16):
    X = np.zeros((len(image_paths), vec_length))
    for i in tqdm(range((len(image_paths) - 1) // batch_size + 1)):
        start_i = i * batch_size
        end_i = start_i + batch_size
        list_of_images = [Image.open(img_path) for img_path in image_paths[start_i:end_i]]

        # print(i,start_i, end_i)
        X[start_i:end_i, :] = img2vec.get_vec(list_of_images)

    return X


def train_classifier(x_train, y_train):
    # create KNN and fit
    clf = neighbors.KNeighborsClassifier()
    clf.fit(x_train, y_train)

    return clf


def wrongly_classified_images(predictions, ground_truth, image_paths):
    file_names = np.asarray(image_paths)[predictions != ground_truth]

    with open('wrongly_classified_images.txt', 'a+') as the_file:
        for line in list(file_names):
            the_file.write(line + "\n")


def run_test(X_train, X_test, in_cart_items, removal_ranges, insertion_labels, removal_labels, current_sample_size, removal_image_paths, current_trial_num, frame_df, majority_df):
    # iterate over all items in cart, removing them one-by-one with
    # replacement
    framewise_average_f1_score = 0
    majority_class_average_accuracy = 0

    # train classifer
    clf = train_classifier(X_train, insertion_labels)

    for item in range(len(in_cart_items)):
        print("Testing current item : {}".format(in_cart_items[item]))

        # get ranges of where the current item is in X_test, and removal_labels
        start, end = removal_ranges[item][0], removal_ranges[item][1]

        # now get the items features and labels
        x = X_test[start : end]
        y = removal_labels [start : end]

        # predict
        predictions = clf.predict(x)

        # y_test = removal_labels[]

        framewise_average_f1_score += (f1_score(y, predictions, labels=[item], average=None)[0])/current_sample_size

        majority_class_average_accuracy += int(majority_vote(predictions) == majority_vote(y))/current_sample_size

        wrongly_classified_images(predictions, y, removal_image_paths[start:end])

    frame_df.loc[current_sample_size][current_trial_num] = framewise_average_f1_score
    majority_df.loc[current_sample_size][current_trial_num] = majority_class_average_accuracy

    return frame_df, majority_df


# create data frames to store info of experiments
framewise_exp_df = create_dataframe(sample_items, sample_items_rate)
majority_vote_exp_df = create_dataframe(sample_items, sample_items_rate)

for sample in sample_items:  # number of items in cart for this testing phase
    for trial in range(sample_items_rate):  # how many times to perform experiment with this number of items in cart
        in_cart_items = get_random_sample_of_items(total_inserted_items, sample)

        print("----"*5 + "\n")
        print("Trial#{}---In-cart items: {} \nNum of items: {} \n\n".format(trial, in_cart_items, len(in_cart_items)))
        print("----" * 5 + "\n")

        # Create Dataset of in cart items
        print("Creating dataset...")

        # image_path -> contains all image paths
        # ranges -> defines from what index ranges in image_paths does item x reside
        # labels -> the label corresponding to the items in image_path
        print("     Creating insertion dataset...")

        insertion_ranges, insertion_labels, insertion_image_paths = create_dataset(insertion_path, in_cart_items)

        print("     Creating removal dataset...")

        removal_ranges, removal_labels , removal_image_paths = create_dataset(removal_path, in_cart_items)

        # Creating Deep features
        print("Creating deep features...")
        X_train = create_deep_features(insertion_image_paths, vec_length, batch_size=16)
        X_test = create_deep_features(removal_image_paths, vec_length, batch_size=16)


        # iterate over all items in cart:
        print('Running Tests ...')
        framewise_exp_df, majority_vote_exp_df = run_test(X_train, X_test, in_cart_items, removal_ranges,
                                                          insertion_labels, removal_labels, sample, removal_image_paths,
                                                          trial+1, framewise_exp_df, majority_vote_exp_df)



framewise_exp_df.to_csv("Frame_wise_exps.csv")
majority_vote_exp_df.to_csv("Majority_vote_exps.csv")




