from utils.mqtt_reader import MqttReader
from utils.helper_funcs import *
import sys
from pathlib import Path
from utils.config import CROPS_STORAGE

"""
example MQTT message
{
    "actualWeight": 330,
    "catalogWeight": 336,
    "confidence": 0.0,
    "gesture": "INSERTED",
    "id": "4daa0251-58aa-442b-a125-1c0d451a2a4f",
    "pipeline": "VISION",
    "productId": "8964000064207",
    "quantity": 1,
    "savedClipsPath": "",  # /home/fovea/4daa0251-58aa-442b-a125-1c0d451a2a4f 
    "timestampUtc": 1583928167.6480727,
    "topProductsResNet": [
        "5053827165853",
        "8964000064207",
        "850563002733"
    ],
    "topProductsWeight": [
        "8964000064955",
        "8964000064702",
        "8964000064207"
    ]
}
"""

mqtt_reader = MqttReader.getInstance()

in_cart_items = []
crops_path = Path(CROPS_STORAGE["crops_path"])

set_aside_path = Path(CROPS_STORAGE["set_aside_path"])

while True:
    try:
        gesture_response = mqtt_reader.read_gesture()

        if gesture_response["gesture"] == "INSERTED":
            # get a list of in-cart items
            in_cart_items = os.listdir(crops_path)
            print("In-Cart Items: {}".format(in_cart_items))
        elif gesture_response["gesture"] == "REMOVED":
            removed_item = gesture_response['id']  # crop dirs are name by uid's
            # Create Dataset of in cart items
            print("Creating dataset...")

            print("     Creating insertion dataset...")
            insertion_ranges, insertion_labels, insertion_image_paths = create_dataset(crops_path, in_cart_items)

            print("     Creating removal dataset...")
            removal_ranges, _, removal_image_paths = create_dataset(crops_path, [removed_item])

            # Creating Deep features
            print("Creating deep features...")
            X_train = create_deep_features(insertion_image_paths, vec_length, batch_size=16)
            X_test = create_deep_features(removal_image_paths, vec_length, batch_size=16)

            # train KNN
            print("Training Classifier...")
            clf = train_classifier(X_train, insertion_labels)

            # predict
            print("Predicting...")
            predictions = clf.predict(X_test)

            matched_with = in_cart_items[majority_vote(predictions)]

            # move both the crops to set_aside_path
            set_aside(insertion_corp_path=crops_path / matched_with, removal_crop_path=crops_path/removed_item,
                      new_dir_name=matched_with, set_aside_path=CROPS_STORAGE["set_aside_path"])

    except KeyboardInterrupt:
        print("KeyboardInterrupt")
        sys.exit()
