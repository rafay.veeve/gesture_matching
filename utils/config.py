MQTT = {
    "mqtt_broker": 'localhost',
    "mqtt_port": 1883,
    "gesture_message_topic": 'cart/events/vision/gestures',
    # "weight_topic_name": 'system/sensors/weight/processed/grams',
    # "barcode_topic_name": 'system/barcodereader',
    # "resolution_topic_name": 'cart/events/vision/message',
    # "plu_topic_name": 'plucode',
    # "error_topic_name": 'cart/events/vision/error'
}


CROPS_STORAGE = {
    "crops_path": "/home/crops/",
    "set_aside_path": "/home/set_aside/"
}

