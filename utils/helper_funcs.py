import numpy as np
from PIL import Image
import glob
import os
from utils.deep_feature_extractor import Img2Vec
from sklearn import neighbors
from shutil import move

img2vec = Img2Vec(model="resnet-50", cuda=True)
vec_length = img2vec.layer_output_size

def majority_vote(predictions):
    return max(zip(*np.unique(predictions, return_counts=True)), key=lambda x: x[1])[0]


def set_aside(insertion_corp_path, removal_crop_path, new_dir_name, set_aside_path):

    # move insertion dir
    dir_name = "{}_{}".format(new_dir_name, "insertion")
    move(insertion_corp_path, set_aside_path/new_dir_name/dir_name)

    # move removal dir
    dir_name = "{}_{}".format(new_dir_name, "removal")
    move(removal_crop_path, set_aside_path/new_dir_name/dir_name)

    print("Crops Moved!")


def create_dataset(input_path, in_cart_items):
    ranges = []
    labels = np.array([], dtype=np.uint)
    image_paths = []
    start = 0

    for i in range(len(in_cart_items)):
        paths = glob.glob(os.path.join(input_path / in_cart_items[i], '*rgb.png'))
        print(len(paths))
        image_paths += paths
        labels = np.concatenate((labels, np.full(shape=len(paths), fill_value=i, dtype=np.uint)))
        ranges.append((start, start + len(paths)))
        start = ranges[-1][-1]

    return ranges, labels, image_paths


def create_deep_features(image_paths, vec_length, batch_size=16):
    X = np.zeros((len(image_paths), vec_length))
    for i in range((len(image_paths) - 1) // batch_size + 1):
        start_i = i * batch_size
        end_i = start_i + batch_size
        list_of_images = [Image.open(img_path) for img_path in image_paths[start_i:end_i]]

        # print(i,start_i, end_i)
        X[start_i:end_i, :] = img2vec.get_vec(list_of_images)

    return X


def train_classifier(x_train, y_train):
    # create KNN and fit
    clf = neighbors.KNeighborsClassifier()
    clf.fit(x_train, y_train)

    return clf