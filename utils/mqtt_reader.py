import logging
import threading
import json
from queue import SimpleQueue
import paho.mqtt.subscribe as subscribe
import utils.config as Config


class MqttReader:
    _instance = None
    @staticmethod
    def getInstance():
        """ Static access method. """
        if MqttReader._instance is None:
            MqttReader()
        return MqttReader._instance

    def __init__(self):
        if MqttReader._instance is not None:
            raise Exception("This class is a singleton!")
        else:
            MqttReader._instance = self
            # self._resolution_topic = Config.MQTT["resolution_topic_name"]
            # self._plu_topic = Config.MQTT["plu_topic_name"]
            # self._barcode_topic = Config.MQTT["barcode_topic_name"]
            # self._weight_topic = Config.MQTT["weight_topic_name"]
            self._gesture_topic = Config.MQTT["gesture_message_topic"]  # my add
            self.broker = Config.MQTT["mqtt_broker"]
            # self._plu_queue = SimpleQueue()
            # self._resolution_queue = SimpleQueue()
            # self._barcode_queue = SimpleQueue()
            # self._weight_queue = SimpleQueue()
            self._gesture_queue = SimpleQueue()  # my add
            self.topics = [self._resolution_topic, self._plu_topic, self._weight_topic, self._barcode_topic]
            t_reader_thread = threading.Thread(
                target=self._reader_thread
            )
            t_reader_thread.daemon = True
            t_reader_thread.start()

    def _reader_thread(self):
        try:
            while True:
                response = json.loads(
                    subscribe.simple(
                        self.topics, hostname=self.broker,
                        retained=False, msg_count=1
                    ).payload.decode('utf-8').replace("'", '"')
                )
                self.publish_message(response)
        except KeyboardInterrupt:
            logging.critical('Exiting through keyboard interrupt')

    def publish_message(self, response):
        # event_type = response["eventType"]
        # if event_type == "WEIGHT_SCALE":
        #     current_weight = response['currentReading']
        #     prev_weight = response['previousReading']
        #     weight_delta = current_weight - prev_weight
        #     self._weight_queue.put((prev_weight, current_weight, weight_delta))
        # if event_type == "BARCODE":
        #     self._barcode_queue.put(response)
        # if event_type == "RESOLVE_ERROR":
        #     self._resolution_queue.put(response)
        # if event_type == "PLU":
        #     if "barcode" in response:
        #         self._barcode_queue.put(response)
        #     else:
        #         self._plu_queue.put(response)
        self._gesture_queue.put(response)


    # my add
    def read_gesture(self):
        if self._gesture_queue.empty():
            return None
        else:
            return self._gesture_queue.get()

    # def read_plu(self):
    #     if self._plu_queue.empty():
    #         return None
    #     else:
    #         return self._plu_queue.get()
    #
    # def read_resoloution_message(self):
    #     if self._resolution_queue.empty():
    #         return None
    #     else:
    #         return self._resolution_queue.get()
    #
    # def read_weight(self):
    #     if self._weight_queue.empty():
    #         return None
    #     else:
    #         return self._weight_queue.get()
    #
    # def read_barcode(self):
    #     if self._barcode_queue.empty():
    #         return None
    #     else:
    #         return self._barcode_queue.get()

    # to remove all responses
    def empty(self):
        # while not self._plu_queue.empty():
        #     self._plu_queue.get()
        # while not self._resolution_queue.empty():
        #     self._resolution_queue.get()
        # while not self._weight_queue.empty():
        #     self._weight_queue.get()
        # while not self._barcode_queue.empty():
        #     self._barcode_queue.get()
        # my add
        while not self._gesture_queue.empty():
            self._gesture_queue.get()
